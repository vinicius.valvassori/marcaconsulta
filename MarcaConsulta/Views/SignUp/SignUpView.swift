//
//  SignUpView.swift
//  MarcaConsulta
//
//  Created by Vinicius on 12/01/24.
//

import SwiftUI

struct SignUpView: View {
    @StateObject var viewModel = ViewModel()
    let labelWidth: CGFloat = 100.0
    
    var body: some View {
        VStack {
            titleView
            formView
        }
    }
    
    private var titleView: some View {
        Text("Criar cadastro")
            .font(.system(size: 30, weight: .bold))
    }
    
    private var formView: some View {
        Form {
            Section {
                HStack {
                    Text("Nome")
                        .frame(width: labelWidth, alignment: .leading)
                    TextField("Nome", text: $viewModel.cpf)
                        .keyboardType(.numberPad)
                }
                HStack {
                    Text("Sobrenome")
                        .frame(width: labelWidth, alignment: .leading)
                    TextField("Sobrenome", text: $viewModel.surname)
                        .keyboardType(.numberPad)
                }
                HStack {
                    Text("CPF")
                        .frame(width: labelWidth, alignment: .leading)
                    TextField("CPF", text: $viewModel.cpf)
                        .keyboardType(.numberPad)
                }
                HStack {
                    Text("E-mail")
                        .frame(width: labelWidth, alignment: .leading)
                    SecureField("E-mail", text: $viewModel.email)
                }
                HStack {
                    Text("Senha")
                        .frame(width: labelWidth, alignment: .leading)
                    SecureField("Senha", text: $viewModel.password)
                }
                HStack {
                    Text("Repetir a senha")
                        .frame(width: labelWidth, alignment: .leading)
                    SecureField("Repetir a senha", text: $viewModel.passwordVerify)
                }
            }
            Button {
                print("viewModel.signUp()")
            } label: {
                Text("Sign Up")
                    .font(.system(size: 20, weight: .bold))
            }
            .padding()
            .frame(maxWidth: .infinity)
            .background(Color.black)
            .foregroundColor(.white)
            //        .opacity(buttonOpacity)
            //.disabled(!viewModel.formIsValid)
        }
    }
}

#Preview {
    SignUpView()
}
