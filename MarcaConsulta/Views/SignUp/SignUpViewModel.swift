//
//  SignUpViewModel.swift
//  MarcaConsulta
//
//  Created by Vinicius on 12/01/24.
//

import Foundation

extension SignUpView {
    @MainActor class ViewModel: ObservableObject {
        enum ViewState {
            case start
            case loading
            case success
            case failure
        }

        @Published var name: String = ""
        @Published var surname: String = ""
        @Published var cpf: String = ""
        @Published var email: String = ""

        @Published var passwordVerify: String = ""
        @Published var password: String = ""

    }
}
