//
//  MarcaConsultaApp.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//

import SwiftUI

@main
struct MarcaConsultaApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
