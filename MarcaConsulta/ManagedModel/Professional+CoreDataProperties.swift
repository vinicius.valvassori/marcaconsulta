//
//  Professional+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension Professional {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Professional> {
        return NSFetchRequest<Professional>(entityName: "Professional")
    }

    @NSManaged public var name: String
    @NSManaged public var councilNumber: Int64
    @NSManaged public var surname: String
    @NSManaged public var specialty: NSOrderedSet
    @NSManaged public var clinic: NSOrderedSet
    @NSManaged public var council: Council?

}

// MARK: Generated accessors for specialty
extension Professional {

    @objc(insertObject:inSpecialtyAtIndex:)
    @NSManaged public func insertIntoSpecialty(_ value: Specialty, at idx: Int)

    @objc(removeObjectFromSpecialtyAtIndex:)
    @NSManaged public func removeFromSpecialty(at idx: Int)

    @objc(insertSpecialty:atIndexes:)
    @NSManaged public func insertIntoSpecialty(_ values: [Specialty], at indexes: NSIndexSet)

    @objc(removeSpecialtyAtIndexes:)
    @NSManaged public func removeFromSpecialty(at indexes: NSIndexSet)

    @objc(replaceObjectInSpecialtyAtIndex:withObject:)
    @NSManaged public func replaceSpecialty(at idx: Int, with value: Specialty)

    @objc(replaceSpecialtyAtIndexes:withSpecialty:)
    @NSManaged public func replaceSpecialty(at indexes: NSIndexSet, with values: [Specialty])

    @objc(addSpecialtyObject:)
    @NSManaged public func addToSpecialty(_ value: Specialty)

    @objc(removeSpecialtyObject:)
    @NSManaged public func removeFromSpecialty(_ value: Specialty)

    @objc(addSpecialty:)
    @NSManaged public func addToSpecialty(_ values: NSOrderedSet)

    @objc(removeSpecialty:)
    @NSManaged public func removeFromSpecialty(_ values: NSOrderedSet)

}

// MARK: Generated accessors for clinic
extension Professional {

    @objc(insertObject:inClinicAtIndex:)
    @NSManaged public func insertIntoClinic(_ value: Clinic, at idx: Int)

    @objc(removeObjectFromClinicAtIndex:)
    @NSManaged public func removeFromClinic(at idx: Int)

    @objc(insertClinic:atIndexes:)
    @NSManaged public func insertIntoClinic(_ values: [Clinic], at indexes: NSIndexSet)

    @objc(removeClinicAtIndexes:)
    @NSManaged public func removeFromClinic(at indexes: NSIndexSet)

    @objc(replaceObjectInClinicAtIndex:withObject:)
    @NSManaged public func replaceClinic(at idx: Int, with value: Clinic)

    @objc(replaceClinicAtIndexes:withClinic:)
    @NSManaged public func replaceClinic(at indexes: NSIndexSet, with values: [Clinic])

    @objc(addClinicObject:)
    @NSManaged public func addToClinic(_ value: Clinic)

    @objc(removeClinicObject:)
    @NSManaged public func removeFromClinic(_ value: Clinic)

    @objc(addClinic:)
    @NSManaged public func addToClinic(_ values: NSOrderedSet)

    @objc(removeClinic:)
    @NSManaged public func removeFromClinic(_ values: NSOrderedSet)

}

extension Professional: Identifiable {

}
