//
//  Appointment+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension Appointment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Appointment> {
        return NSFetchRequest<Appointment>(entityName: "Appointment")
    }

    @NSManaged public var dateTime: Date
    @NSManaged public var professional: Professional?
    @NSManaged public var specialty: Specialty
    @NSManaged public var status: AppointmentStatus
    @NSManaged public var clinic: Clinic

}

extension Appointment: Identifiable {

}
