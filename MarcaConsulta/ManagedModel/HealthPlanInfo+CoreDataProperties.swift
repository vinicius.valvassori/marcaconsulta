//
//  HealthPlanInfo+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 12/01/24.
//
//

import Foundation
import CoreData

extension HealthPlanInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HealthPlanInfo> {
        return NSFetchRequest<HealthPlanInfo>(entityName: "HealthPlanInfo")
    }

    @NSManaged public var name: String?
    @NSManaged public var info: String?
    @NSManaged public var product: String?
    @NSManaged public var ansNumber: String?
}

extension HealthPlanInfo: Identifiable {}
