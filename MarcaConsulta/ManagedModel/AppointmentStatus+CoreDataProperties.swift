//
//  AppointmentStatus+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension AppointmentStatus {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppointmentStatus> {
        return NSFetchRequest<AppointmentStatus>(entityName: "AppointmentStatus")
    }

    @NSManaged public var title: String
}

extension AppointmentStatus: Identifiable {

}
