//
//  Clinic+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension Clinic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Clinic> {
        return NSFetchRequest<Clinic>(entityName: "Clinic")
    }

    @NSManaged public var name: String
    @NSManaged public var image: String?
    @NSManaged public var responsible: Professional?
    @NSManaged public var professionals: NSOrderedSet
    @NSManaged public var address: Address

}

// MARK: Generated accessors for professionals
extension Clinic {

    @objc(insertObject:inProfessionalsAtIndex:)
    @NSManaged public func insertIntoProfessionals(_ value: Professional, at idx: Int)

    @objc(removeObjectFromProfessionalsAtIndex:)
    @NSManaged public func removeFromProfessionals(at idx: Int)

    @objc(insertProfessionals:atIndexes:)
    @NSManaged public func insertIntoProfessionals(_ values: [Professional], at indexes: NSIndexSet)

    @objc(removeProfessionalsAtIndexes:)
    @NSManaged public func removeFromProfessionals(at indexes: NSIndexSet)

    @objc(replaceObjectInProfessionalsAtIndex:withObject:)
    @NSManaged public func replaceProfessionals(at idx: Int, with value: Professional)

    @objc(replaceProfessionalsAtIndexes:withProfessionals:)
    @NSManaged public func replaceProfessionals(at indexes: NSIndexSet, with values: [Professional])

    @objc(addProfessionalsObject:)
    @NSManaged public func addToProfessionals(_ value: Professional)

    @objc(removeProfessionalsObject:)
    @NSManaged public func removeFromProfessionals(_ value: Professional)

    @objc(addProfessionals:)
    @NSManaged public func addToProfessionals(_ values: NSOrderedSet)

    @objc(removeProfessionals:)
    @NSManaged public func removeFromProfessionals(_ values: NSOrderedSet)

}

extension Clinic: Identifiable {

}
