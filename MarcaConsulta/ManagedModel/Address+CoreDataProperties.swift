//
//  Address+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension Address {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address")
    }

    @NSManaged public var city: String?
    @NSManaged public var country: String?
    @NSManaged public var district: String?
    @NSManaged public var extra: String?
    @NSManaged public var name: String?
    @NSManaged public var number: Int64
    @NSManaged public var zipCode: Int64
    @NSManaged public var state: String?
    @NSManaged public var addressType: AddressType?
}

extension Address: Identifiable {

}
