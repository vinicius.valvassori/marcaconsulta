//
//  Specialty+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension Specialty {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Specialty> {
        return NSFetchRequest<Specialty>(entityName: "Specialty")
    }

    @NSManaged public var details: String?
    @NSManaged public var title: String

}

extension Specialty: Identifiable {

}
