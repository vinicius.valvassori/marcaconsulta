//
//  Council+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension Council {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Council> {
        return NSFetchRequest<Council>(entityName: "Council")
    }

    @NSManaged public var name: String?
    @NSManaged public var acronym: String

}

extension Council: Identifiable {

}
