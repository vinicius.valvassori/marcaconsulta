//
//  AddressType+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 11/01/24.
//
//

import Foundation
import CoreData

extension AddressType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AddressType> {
        return NSFetchRequest<AddressType>(entityName: "AddressType")
    }

    @NSManaged public var title: String
}

extension AddressType: Identifiable {

}
