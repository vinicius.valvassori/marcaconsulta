//
//  Patient+CoreDataProperties.swift
//  MarcaConsulta
//
//  Created by Vinicius on 12/01/24.
//
//

import Foundation
import CoreData

extension Patient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Patient> {
        return NSFetchRequest<Patient>(entityName: "Patient")
    }

    @NSManaged public var name: String
    @NSManaged public var surname: String
    @NSManaged public var age: Int64
    @NSManaged public var phoneNumber1: String?
    @NSManaged public var phoneNumber2: String?
    @NSManaged public var cpf: String
    @NSManaged public var cnsNumber: String?
    @NSManaged public var recipientNumber: String?
    @NSManaged public var isHolder: Bool
    @NSManaged public var address: Address?
    @NSManaged public var accountHolder: Patient?
    @NSManaged public var healthPlan: HealthPlanInfo?
}

extension Patient: Identifiable {
}
